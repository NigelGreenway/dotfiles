#!/usr/bin/sh
ORIGINAL_DIRECTORY=./originals
PRE_INSTALLED_ZSH_CONFIG=$(cat ./templates/zshrc.tpl)
INSTALLED_ZSH_CONFIG="${PRE_INSTALLED_ZSH_CONFIG/__DOTFILES_PATH__/$PWD}"

LINUX_FLAVOUR=$(cat /etc/*-release | grep ^NAME=\" | cut -d '=' -f2 | sed s/\"//g)

if [[ "$LINUX_FLAVOUR" == "Solus" ]]
then
  source ./solus.install
  installPackages
fi

echo "Installing oh-my-zsh"
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "Installing pm"
cd ~
wget https://raw.githubusercontent.com/Angelmmiguel/pm/master/install.sh
chmod 755 ./install.sh
. ./install.sh
cd -

if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "Installing Homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

echo "$INSTALLED_ZSH_CONFIG" > ./.zshrc

if [ ! -d "$ORIGINAL_DIRECTORY" ]; then
  mkdir "$ORIGINAL_DIRECTORY"
fi

for file in $(find . -type f -path "./.*" -not -path "./.git/*" -not -path "./.gitignore" -not -path ./templates/*)
do
  host_original_file="$HOME/${file/.\//}"
  backup_direction_for_host_version="./originals${file/.\//\/}"
  move_directory=${backup_direction_for_host_version%/*}

  install_to="${host_original_file}"
  install_directory="${install_to%/*}"

  if ! test -f ${backup_direction_for_host_version}; then
    if [[ ${install_directory} != $HOME ]]; then
      mkdir -p ${install_directory}
    fi

    if [[ ${move_directory} != "./originals/${file}" ]]; then
      mkdir -p ${move_directory}
    fi

    mv $host_original_file $backup_direction_for_host_version
    cp $file $install_to
    installed_file=$(basename $file)
    echo -e "\e[32m\e[1m${installed_file}\e[0m has been installed"
  else
    echo -e "\e[31m\e[1m${file}\e[0m has already been installed"
  fi
done

mv ./.zshrc ~/.zshrc
rm ./.zshrc
