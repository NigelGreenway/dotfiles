# dotfiles

My personal space for my local configuration for my digital toolset.

This will eventually pull in the following configs:

 - [Vim](https://gitlab.com/nigelgreenway/vim)
 - [Installer](https://gitlab.com/nigelgreenway/installer)

## Setting up the dotfiles

To install the dotfiles run `sh ./install.sh`.  

This will take all the files in this repo and then check if it already exists on the target host. If they do exist, they will be backed up to `./originals`.
## Removing the dotfiles


To remove the dotfiles and revert to the originals, run `sh ./remove.sh` and this will remove your dot files and replace with the originals in the `./originals` directory.

## backup the root dotfiles

To backup the dotfiles, run `sh ./backup.sh`

When you make changes, your dotfiles can quickly get out of date when you make adhoc changes.

The backup utility will allow you to move the changed files into your git project and stage the files as you need.

If the file already has changes it will warn you and not overwrite your current changes until you've stashed them, reverted or committed.
