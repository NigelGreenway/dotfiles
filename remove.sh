# !/bin/env zsh
# Remove personal dotfiles

if ! test -d ./originals; then
  echo -e "\e[31m\e[1mOriginal directory doesn't exist"
  exit 1
fi

for file in $(find ./originals -type f)
do
  move_to="$HOME/${file/.\/originals\//}"

  if test -f "$move_to"; then
    rm -rf "$move_to"
    mv "$file" "$move_to"
  fi
done

rm -rf ./originals

echo -e "\e[32mDotfiles successfully restored"
