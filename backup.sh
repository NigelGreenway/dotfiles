# !/bin/env bash

function confirmation_prompt {
  read -r -p "Are you sure? [y/N] " response
  case "$response" in
    [yY][eE][sS]|[yY]) 
      $@
      ;;
    *)
      exit 1;
  esac
}

function commit_backup_changes {

  local isNewBranch=false

  read -r -p "Would you like these changes in a new branch? [y/N] " create_branch

  read -r -p "Please summarise your changes: " summary_of_changes

  case "$create_branch" in
    [yY][eE][sS]|[yY])
      NEW_BRANCH_NAME=${summary_of_changes// /$'-'}
      git checkout -b "${NEW_BRANCH_NAME}"
      isNewBranch=true
      ;;
    *)
  esac

  git add -p .
  git commit -m "$summary_of_changes"

  read -r -p "Would you like the branch pushed? [y/N] " push_branch

  case "$push_branch" in
    [yY][eE][sS]|[yY])
      if $isNewBranch; then
        git push -u origin $NEW_BRANCH_NAME
      else
        git push --force-with-lease
      fi
      ;;
    *)
      exit 1;
  esac

  git checkout -

  ORIGIN_REMOTE_URL=$(git config --get remote.origin.url)
  ORIGIN_REMOTE_URL=${ORIGIN_REMOTE_URL/git@/}
  ORIGIN_REMOTE_URL=${ORIGIN_REMOTE_URL/://}

  echo "Your branch is ready for you ;)"
  echo "https://${ORIGIN_REMOTE_URL}"
}

function backup_files {
  for file in $(find . -type f -path "./.*" -not -path "./.git/*" -not -path "./.gitignore")
  do
    root_version=${file/.\///}
    tracked_version=$DOTFILES_PATH$root_version

    if test -f "$HOME$root_version"; then
      if [[ $(git status --porcelain ${tracked_version} | awk '{print $2}') == $tracked_version ]]; then
        echo -e "\e[31m${tracked_version} is already staged, please stash or commit your changes"
      else 
        echo -e "\e[32mbacking up ${tracked_version}\e[0m"
        cp -r "$HOME$root_version" $tracked_version
      fi
    fi
  done

  if [[ $(git status --porcelain | grep -E "^M|M" | wc -l) -gt 0 ]]; then
    commit_backup_changes
  fi

  echo -e "\e[32mDotfiles successfully backed up"
}

function run {
  if [[ -d "./.git" ]]; then

    if [ -v $DOTFILES_PATH ]; then
      echo -e "\n\n\n\e[31mThe environment variable DOTFILES_PATH doesn't exist"
      exit 1
    fi

    backup_files
  else
    echo -e "\e[31mYou're not in a git tracked project so you changes will be overwritten in $PWD\e[0m"

    confirmation_prompt backup_files
  fi
}

echo -e "\e[31mYou are just about to overwrite your personal dotfiles with the hosts version of the tracked dot files.\e[0m"
confirmation_prompt run
